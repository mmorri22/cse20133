/**********************************************
* File: lltest.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the main driver function
* for a Linked List test 
**********************************************/

#include "linkedlist.h"

/********************************************
* Function Name  : deleteList
* Pre-conditions : LinkedList<T>& theList
* Post-conditions: void
* 
* While the list is not empty, the function 
* calls the 
********************************************/
template<class T>
void deleteList(LinkedList<T>& theList){
	
	T scanVal;
	
	while(theList.IsEmpty() != 1){

		std::cout << "\nEnter a value to be deleted :";
		std::cin >> scanVal;
		
		theList.Delete(scanVal);
		
		std::cout << theList << std::endl;
	
	}
	
}

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
* For this lecture, we assume 
********************************************/
int main ()
{

	/* Call default constructor for Linked List */
	LinkedList<int> intList;
	
	/* Start at the first int, and go until the end of the command line */
	intList.Insert(10);
	intList.Insert(25);
	intList.Insert(12);
	
	std::cout << intList << std::endl;
	
	/* Pointer to prtList calls the copy constructor */
	LinkedList<int>* ptrList = new LinkedList<int>(intList);
	std::cout << ptrList->getHead() << " " << intList.getHead() << std::endl;
	
	/* Print the list to the user. Will print empty since a pointer */
	std::cout << "ptrList: " << *ptrList << std::endl;
	deleteList(*ptrList);
	delete ptrList;
	
	/* Call default constructor for Linked List */
	LinkedList<float> floatList;
	floatList.Insert( (float) 1.1);
	floatList.Insert( (float)-2.3);
	floatList.Insert( (float)6.5);
	
	/* Print the list to the user */
	std::cout << "floatList: " << floatList << std::endl;
	deleteList(floatList);
	
	/* Call default constructor for Linked List */
	LinkedList< std::string > strList;
	strList.Insert(std::string("Let's"));
	strList.Insert(std::string("Go"));
	strList.Insert(std::string("Irish"));
	
	/* Print the list to the user */
	std::cout << "strList: " << strList << std::endl;
	deleteList(strList); 

    return 0;
}
