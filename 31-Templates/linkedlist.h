/**********************************************
* File: linkedlist.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the function definitions and 
* structs for a Linked List Data structure 
**********************************************/

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stdlib.h>
#include <iostream>
#include <string>


/* NODE contains the data and a pointer to the next node */
template<class T>
class node
{
	public:
		T     data;
		node<T>* next;    /* OR struct node *next; */
		
		node() : data(), next(NULL) {}
		
		~node(){
			next = NULL;
		}
		
		node(const node<T>& rhs){
			data = rhs.data;
			next = rhs.next;
		}
		
		node<T> operator=(const node<T>& rhs){
			
			if(this != &rhs){
		
				this.data = rhs.data;
				this.next = rhs.next;
			
			}
			
			return *this;
		}
	
};


template<class T>
class LinkedList{
	
	private:
		node<T>* head;
		
		/********************************************
		* Function Name  : CreateNode
		* Pre-conditions : void
		* Post-conditions: node<T>*
		* 
		* CreateNode initializes the members
		* CreateNode exits if there is insufficient
		* memory
		********************************************/
		node<T>* CreateNode (void){
			
		   node<T>* newNode = new node<T>();

		   /* Get the space needed for the node */
		   //newNode = new node<T>*();
		   
		   if (newNode == NULL)
		   {
			  fprintf (stderr, "Out of Memory - CreateNode\n");
			  exit (-1);
		   }

		   /* Initialize the members */
		   newNode -> next = NULL;

		   return newNode;
			
		}
		
		
		node<T>* getHeadNode() const{
			return head;
		}
		
	public:
	
		LinkedList() : head(NULL) {}

		LinkedList(const LinkedList<T>& copy) : head(copy.head) { }		
		
		~LinkedList(){
			
			delete head;
				
		}
		
		LinkedList<T>& operator=(const LinkedList<T>& assign) {

			if(this != &assign){
				node<T>* curr = CreateNode();	
				
				//prev = NULL;
				curr = assign.head;

				while (curr != NULL)
				{
					Insert(curr->data);
					curr = curr -> next;
				}
			}
			
			return *this;
			
		}	

		/********************************************
		* Function Name  : Insert
		* Pre-conditions : T value
		* Post-conditions: void   
		* 
		* the node is inserted at the end of the linked list
		********************************************/
		void    Insert     (T value)
		{
	
		   node<T>* temp = CreateNode();
		   temp -> data = value;
		   temp -> next = NULL;
		   
		   node<T>* prev = CreateNode(); 
		   node<T>* curr = CreateNode();

		   if ( IsEmpty ())
		   {
			   head = temp;
		   }
		   else
		   {
			  prev = NULL;
			  curr = head;

			  /* traverse the list until the end */
			  while (curr != NULL)
			  {
				 prev = curr;
				 curr = curr -> next;
			  }
		  
			  /* insert the node, temp, at the end */
			  prev -> next = temp;
		   }
		   
		   curr = NULL;
		   delete curr;
		}


		/********************************************
		* Function Name  : Delete
		* Pre-conditions : T data
		* Post-conditions: T    
		*  
		* Delets the first instance of data. Returns -1 
		* if not found
		********************************************/
		void     Delete (T target)
		{
		   node<T>* temp = CreateNode();
		   node<T>* prev = CreateNode();
		   node<T>* curr = CreateNode();

		   if (IsEmpty ())
		   {
			  printf ("Can't delete from an empty list\n");
		   }

		   /* if the target value is the first
		   in the list, move head */
		   else if (target == head -> data)
		   {
			  temp = head;
			  head = head -> next;
			  delete temp;
		   }
		   
		   /* traverse the list until the 
		   target value is found */
		   else
		   {
			  prev = head;
			  curr = head -> next;

			  while (curr != NULL &&
				 curr -> data != target)
			  {
				 prev = curr;
				 curr = curr -> next;
			  }
			  
			  if(curr != NULL)
			  {
			
				/* delete the node the contains
				the target value */
				temp = curr;
				prev -> next = curr -> next;
				delete temp;
			  }
			  else
			  {
				std::cout << target << " was not in the list\n";
			  }
		   }  

		}


		/********************************************
		* Function Name  : IsEmpty
		* Pre-conditions : node<T>* head
		* Post-conditions: int    
		* 
		* returns 1 (true) if the list is empty
		* returns 0 (false) if the list is not empty
		********************************************/
		int     IsEmpty    () const
		{
		   return (head == NULL);
		}

		node<T>* getHead() const{
			return head;
		}		


		/********************************************
		* Function Name  : PrintList
		* Pre-conditions : node<T>* head
		* Post-conditions: void    
		* 
		* each node in the list is printed according to the format specified in this code
		********************************************/
		friend std::ostream& operator<<( std::ostream& output, const LinkedList<T>& theList )
		{
		   node<T>* curr;

		   if (theList.IsEmpty())
		   {
			  output << "The list is empty" << std::endl;;
		   }
		   else
		   {
			  /* set the current pointer to the first
			  ** node of the list */
			  curr = theList.getHeadNode();

			  /* Until the end of the list */
			  while (curr != NULL)
			  {
				  /* print the current data item */
				  output << curr->data << " ";

				  /* move to the next node */
				  curr = curr -> next;
			  }
		   } 

			return output;
		}

};

#endif
