/**********************************************
* File: hello.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains a program to run Hello, World 
**********************************************/

#include <stdio.h> /* Standard I/O Library */ 

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This function is the main driver program 
********************************************/
int main(void){

	fprintf(stdout, "Hello, World\n");

	return 0;

}

