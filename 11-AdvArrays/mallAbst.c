/**********************************************
* File: mallAbst.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This is a program that shows the student how to 
* allocate memory using both char and void pointers 
**********************************************/

#include "stdio.h"
#include "stdlib.h" // Included here for malloc 

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	// Allocate a void pointer using malloc 
	long unsigned int size1 = 5;
	void *vp1 = malloc(size1 * sizeof(char));
	
	// Allocate a void point that is abstracted using char
	long unsigned int size2 = 4;
	char *cp1 = malloc(size2 * sizeof(char));

	// Next, we are going to populate this array 
	// There are examples of explicit AND implicity type casting 
	((char *)vp1)[0] = 'N';
	((char *)vp1)[1] = 111;
	((char *)vp1)[2] = 't';
	((char *)vp1)[3] = 70+44;
	((char *)vp1)[4] = 0x65;	// 0x65 in hex is equal to 101 in decimal
	
	// Allocate the second word 
	cp1[0] = 'D'; cp1[1] = 'a'; cp1[2] = 'm'; cp1[3] = 'e';
	
	fprintf(stdout, "The word is: %s\n", (char *)vp1);
	fprintf(stdout, "The second word is %s\n\n", cp1);
	
	fprintf(stdout, "The base address of cp1 is %p\n", (void *)cp1);
	fprintf(stdout, "The address of cp1[0] is %p\n", (void *)&cp1[0]);
	fprintf(stdout, "The address of cp1[1] is %p\n", (void *)&cp1[1]);	
	fprintf(stdout, "The address of cp1[2] is %p\n", (void *)&cp1[2]);	
	
	// Free all allocated memory 
	free(vp1);
	free(cp1);

	return 0;
}
