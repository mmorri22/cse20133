/**********************************************
* File: separStr.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows a basic example of STL C++ Hash Map
* with Separate Chaining with Strings
**********************************************/
#include <unordered_map>
#include <iterator>
#include <list>
#include <string>
#include <iostream>

const int numBuckets = 8;

int HashFunc(std::string value){
	return value.size() % numBuckets;
}


void insertVal(std::unordered_map<int, std::list< std::string > >& sepChain, std::string value){
	
	sepChain[ HashFunc(value) ].push_back(value);
	
}


void printHash(std::unordered_map<int, std::list< std::string > >& sepChain){

	for(int i = 0; i < numBuckets; i++){
	
		std::cout << i << ": ";
	
		/* sepChain[numBuckets] gives the list to iterate through */
		for(std::string& curr : sepChain[i]){
			std::cout << curr << " ";
		}
		
		std::cout << std::endl;
	}
	
}

/********************************************
* Function Name  : main
* Pre-conditions :
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(){

	std::unordered_map<int, std::list< std::string > > sepChain;
	
	/* Initialize List */
	for(int i = 0; i < numBuckets; i++){
		std::list< std::string > temp;
		sepChain.insert( {i, temp} );
	}
	
	/* Insert words of length 4, 13, 8, 10, 5, 15 */
	insertVal(sepChain, "data");
	insertVal(sepChain, "enlightenment");
	insertVal(sepChain, "morrison");
	insertVal(sepChain, "structures");
	insertVal(sepChain, "pizza");
	insertVal(sepChain, "rumpelstiltskin");

	/* Print the Hash */
	printHash(sepChain);

	return 0;
}
