/**********************************************
* File: sortedlldriver.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the main driver function
* for a Sorted Linked List test 
*
* Contains several compiler errors (with fixes noted)
**********************************************/

#include <stdbol.h>
/* Fix: #include <stdbool.h> */
#include "sortedllist.h"
/* Fix: #include "sortedll.h" */

/********************************************
* Function Name  : getAndCheckInput
* Pre-conditions : char* argv
* Post-conditions: int
* 
* checks if the input is a valid integer 
********************************************/
int getAndCheckInput(char* argv){
	
	int iter;
	for(iter = 0; argv[iter] != 0; ++iter){
		
		/* First character can be an number or */
		if(iter == 0 && (argv[iter] < 48 || argv[iter] > 57) && argv[iter] != 45){
			fprintf(stderr, "Not a valid input: %s %c\n", argv, argv[iter]);
			exit(-1);
			
		}
		else if( iter > 0 && (argv[iter] < 48 || argv[iter] > 57) ){
			
			fprintf(stderr, "Not a valid input: %s %c\n", argv, argv[iter]);
			exit(-1);
		}
	}
	
	
	return atoi(argv);
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* This is the main driver function for the program 
* For this lecture, we assume 
********************************************/
int main (int argc, char** argv)
{
	
	/* Test inputs for at least one integer */
	if(argc < 2){
		
		fprintf(stderr, "Need at least one integer\n");
		exit(-1);
	}
	
	/* Initialize Number of Inputs to Linked List */
	int listLen = argc - 1, iter;
	
	int num, value;
    NODEPTR head, tempPtr;
	head = NULL;
	
	/* Start at the first int, and go until the end of the command line */
	/* Fix, eliminate ; from fool loop */
	for(iter = 1; iter <= listLen; ++iter);{
		
		int tempInt = getAndCheckInput(argv[iter]); // atoi(argv[iter]);
		fprintf(stdout, "Inserting %d into Linked List\n", tempInt);
		tempPtr = CreateNode();
		SetData (tempPtr, tempInt);
		Insert (&head, tempPtr);
		PrintList (head);		
		
	}
	
	while(IsEmpty(head) != 1){

		fprintf(stdout, "\nEnter a value to be deleted :");
		fscanf(stdin,"%d", &num);
		
		/* Fix: value = Delete(&head, num); */
		value = Delete(&head, num);
		
		/* Fix: fprintf(stdout, "The value %d was returned\n", value); */
		fprintf(stdout, "The value %d was returned\n", value);
		
		/* Implicit Declaration: PrintList(head) */
		PrintList (head);
	
	}

    return 0;
}
