/**********************************************
* File: swapArray.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file includes a function that passes an int
* array to a function and then swaps two values
**********************************************/

#include <stdio.h>
#include <stdlib.h> // Included for malloc

void swapInts(int* word, int a, int b){
	
	fprintf(stdout, "Before: word[%d] = %d and word[%d] = %d\n", a, word[a], b, word[b]);
	
	int temp = word[a];
	word[a] = word[b];
	word[b] = temp;
	
	fprintf(stdout, "After: word[%d] = %d and word[%d] = %d\n", a, word[a], b, word[b]);
	
}

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	long unsigned int numInts = 5;
	
	int* SwapInt = malloc(numInts * sizeof(int));
	
	int i;
	for(i = 0; i < (int)numInts; i++){
		SwapInt[i] = i*2;
	}
	
	swapInts(SwapInt, 0, i-1);
	
	free(SwapInt);

	return 0;
}

