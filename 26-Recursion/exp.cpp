/**********************************************
* File: exp.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* The main driver for the exponential recursive
* solver 
**********************************************/
#include <cmath> // pow
#include "Recurse.h"
#include "Supp.h"

int main(int argc, char** argv){
	
	std::cout << exponential(getArgv1Num(argc, argv), 100000) << std::endl;
	
	return 0;
}
