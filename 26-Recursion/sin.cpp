/**********************************************
* File: sin.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This function contains the main driver function
* for doing sin(x) using recursion 
**********************************************/
#include "Supp.h"
#include "Recurse.h"

#include <iostream>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* This is the main driver function 
********************************************/
int main(int argc, char** argv){

	std::cout << sin(getArgv1Num(argc, argv), 20) << std::endl;

	return 0;
}
