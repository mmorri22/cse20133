/**********************************************
* File: survey.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains a program that calculates
* the average and standard deviation based on
* the number of respondents from 1-5
*
* Input: ./survey [QuestionNum] 1 2 3 4 5 
**********************************************/

#include <iostream>
#include <cstdlib>
#include <sstream>	/* Input checking C++ */
#include <cmath> 	/* For pow and sqrt */
#include <iomanip>  /* setprecision */

/********************************************
* Function Name  : getVals
* Pre-conditions : int* answerVals, unsigned long int numVals, char** argv
* Post-conditions: double
* 
* This function calculates the average of the five
* integers and returns a double 
********************************************/
void getVals(int *answerVals, unsigned long int numVals, char** argv, int &totSurveyed){
	
	unsigned long int iter = 0;
	
	/* Get the total number surveyed and sum of values */
	for(iter; iter < numVals; iter++){
		
		std::istringstream ss(argv[ iter + 2 ]);
	
		if( !( ss >> answerVals[iter]) ){
			
			std::cerr << std::cout << "Invalid input. " << argv[ iter + 1 ] << " is not a valid integer" << std::cout;
			exit(-1);
		}
		
		totSurveyed += answerVals[iter];
	
	}
	
}

/********************************************
* Function Name  : average
* Pre-conditions : int* answerVals, unsigned long int numVals
* Post-conditions: double
* 
* This function calculates the average of the five
* integers and returns a double 
********************************************/
double average(int *answerVals, unsigned long int numVals, int totSurveyed){
	
	unsigned long int iter = 0;
	int totVal = 0;
	double average;
	
	/* Get the total number surveyed and sum of values */
	for(iter; iter < numVals; iter++){
		
		totVal += (iter + 1) * answerVals[iter];
	}

	average = (double)totVal / (double)totSurveyed;

	std::cout << "Average = " << std::setprecision(4) << average << ". ";

	return average;
}

/********************************************
* Function Name  : stdDev
* Pre-conditions : int* answerVals, unsigned long int numVals, double average
* Post-conditions: double
* 
* This function calculates the stdDev of the five
* integers and returns a double 
********************************************/
double stdDev(int *answerVals, unsigned long int numVals, double average, int totSurveyed){
	
	unsigned long int iter = 0, jter = 0;
	int totVal = 0;
	double numerator = 0;
	
	/* Get the total number surveyed and sum of values */
	for(iter; iter < numVals; iter++){
		
		for(jter = 0; jter < answerVals[iter]; jter++){
		
			double currDiff = (iter + 1) - average;
			numerator += pow(currDiff, 2);
		
		}
	}

	return sqrt( (double)numerator / (double)totSurveyed );
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(int argc, char** argv){
	
	unsigned long int numVals = 5;
	int totSurveyed = 0;
	
	/* Error-checking. Requires argc to be 7 */
	if(argc != numVals + 2){
		
		std::cerr << "Number of inputs must be 7" << std::endl;
		std::cerr << "./survey [QuestionNum] 1 2 3 4 5 " << std::endl;
		std::exit(-1);
		
	}
	
	int *answerVals = (int *)malloc( numVals * sizeof(int) );
	
	std::cout << "Question " << argv[1] << ": ";
	
	/* Convert inputs to string */
	getVals(answerVals, numVals, argv, totSurveyed);

	/* Calculate Average and Standard Deviation */
	
	std::cout << "Standard Deviation = " << std::setprecision(4) 
		<< stdDev( answerVals, numVals, average( answerVals, numVals, totSurveyed ), totSurveyed ) << ". ";
	
	std::cout << std::endl;
	
	/* Free the allocates array */
	free(answerVals);

	return 0;
}

