/**********************************************
* File: factDouble.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains a program that calculates a
* factorial of a given number and stores the result
* in a double using a for loop 
**********************************************/
#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* Declare variables */
	int num = 171;
	double factorial = 1;
	int iter;
	
	for(iter = 1; iter <= num; iter++){
		
		factorial *= (double)iter;
		
		fprintf(stdout, "%d! = %le. Hex is %la\n", iter, factorial, factorial);
		
	}

	
	return 0;
}
