/**********************************************
* File: factFloat.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains a program that calculates a
* factorial of a given number and stores the result
* in a float using a for loop 
**********************************************/
#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* Declare variables */
	int num = 35;
	float factorial = 1;
	int iter;
	
	for(iter = 1; iter <= num; iter++){
		
		factorial *= (float)iter;
		
		fprintf(stdout, "%d! = %f. Hex is %a\n", iter, factorial, factorial);
		
	}

	
	return 0;
}
