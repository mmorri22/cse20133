/**********************************************
* File: forEx.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains a program that emphasizes to the
* student that for loops should have conditions based
* on a fixed input. Here we know that forInt is 7
* and that the end condition is 0
**********************************************/

#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Declare variables */
	int forInt;
	
	/* For Loop Example */
	for(forInt = 7; forInt > 0; forInt--){
		
		fprintf(stdout, "forInt = %d\n", forInt);
	}

	return 0;
}
