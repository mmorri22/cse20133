echo "Run Missing Input"
./factCmdLine
echo "Too many inputs"
./factCmdLine 10 6
echo "Test for non-number ASCII"
./factCmdLine ygjkfggls
echo "Good initial test"
./factCmdLine 1
echo "Test 12 for Int"
./factCmdLine 12
echo "Test 13 for Int"
./factCmdLine 13
echo "Test 34 for Float"
./factCmdLine 34
echo "Test 35 for Float"
./factCmdLine 35
echo "Test 170 for Double"
./factCmdLine 170
echo "Test 171 for Double"
./factCmdLine 171