/**********************************************
* File: helloCmdLine.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file prints the inputs after the executable
* in the command lind 
**********************************************/

#include <stdio.h>
#include <stdlib.h>

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* This is the main driver function of the program 
********************************************/
int main(int argc, char** argv){
	
	/* Error Checking */
	if(argc < 2){
		fprintf(stderr, "Not enough inputs on command line\n");
		exit(-1);
	}
	
	int i = 0;
	while(i < argc){
		fprintf(stdout, "argv[%d] = %s\n", i, *(argv + i));
		i++;
	}

	return 0;
}
