/**********************************************
* File: unintNullFix.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains an example of an unintentional
* NULL statement to help the new coder troubleshoot 
* And this contains the fixed version
**********************************************/
#include "stdio.h"

int main(void){

	int i = 10;
	
	
	/* Uninitentional nill is removed */
	while(i > 0)
	{
		
		fprintf(stdout, "The value of i is %d\n", i);
		i--;
		
	}


	return 0;
}

