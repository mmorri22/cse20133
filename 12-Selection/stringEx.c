/**********************************************
* File: stringEx.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains fundamental use of string, and how to 
* use multiple output specifiers, and how to access a character 
* in a string to print its char, ASCII, and hex values
**********************************************/
#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	char sExample[] = "Notre";
	char* sExample2 = "Dame";
	
	fprintf(stdout, "The string output for sExample and sExample2 is     : %s %s\n\n", sExample, sExample2);
	
	fprintf(stdout, "The text character value output for sExample[0] is  : %c\n", sExample[0]);		// Will print N 
	fprintf(stdout, "The ASCII int value output for sExample[0] is       : %d\n", sExample[0]);		// Will print 78
	fprintf(stdout, "The Hex int value output for sExample[0] is         : %x\n\n", sExample[0]);	// Will print 4e
	
	fprintf(stdout, "The text character value output for sExample2[1] is : %c\n", sExample2[2]);	// Will print m 
	fprintf(stdout, "The ASCII int value output for sExample2[1] is      : %d\n", sExample2[2]);	// Will print 109
	fprintf(stdout, "The hex value output for sExample2[1] is            : %x\n", sExample2[2]);	// Will print 6d

	return 0;
}

