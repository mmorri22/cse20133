/**********************************************
* File: combine.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains an example of arrays, pointer
* arithmetic, if/else statements, and switch statements 
**********************************************/

#include <stdlib.h>
#include <stdio.h>

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Declare variables */
	int smaller[] = {1, 1842, 2};
	int *larger = malloc(3 * sizeof(int));
	
	/* Insert values into larger individually */
	larger[0] = 1000;
	*(larger + 1) = 0;
	larger[2] = 47577;
	
	int location;
	
	if(smaller[0] > larger[0]){
		location = 0;
	}
	else if(smaller[1] > *(larger + 1)){
		location = 1;
	}
	else if( *(smaller + 2) > larger[2] ){
		location = 2;
	}
	else{
		location = -1;
	}
	
	/* Perform switch statement */
	switch(location){
		
		case 0:
			fprintf(stdout, "Location is 0, %d %d\n", smaller[0], *(larger + 0));
			break;
			
		case 1:
			fprintf(stdout, "Location is 1, %d %d\n", smaller[1], *(larger + 1));
			break;
			
		case 2:
			fprintf(stdout, "Location is 2, %d %d\n", smaller[2], *(larger + 2));
			break;
			
		default:
			fprintf(stdout, "Location not found\n");
			break;
		
	}
	
	
	/* Free allocated memory */
	free(larger);

	return 0;
}