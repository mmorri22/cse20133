/**********************************************
* File: ifElseEx.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains an example of an if/else statement 
**********************************************/
#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Variable Declarations */
	float x = (float)1.1, y = (float)1.2;
	double x2 = 1.3, y2 = 1.4;
	
	/* Evaluae if statements */
	if( x == y ){
		fprintf(stdout, "The statement x==y is true\n");
	}
	else{
		fprintf(stdout, "The statement x==y is false\n");
	}
	
	if( x2 == y2 ){
		fprintf(stdout, "The statement x2==y2 is true\n");
	}
	else if( (y2 - x2) == 0.1 ){
		fprintf(stdout, "The statement (y2 - x2) == 0.1 is true\n");
	}
	else{
		fprintf(stdout, "Both are false! %0.20lf, %a\n", y2 - x2, y2 - x2);
	}
	
	double yourMoney = 100.15;
	yourMoney -= 0.18;
	fprintf(stdout, "Your Money is now %.20lf\n", yourMoney);
	
	

	return 0;
}

