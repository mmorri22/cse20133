/**********************************************
* File: ifEx.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains an example of an if statement 
**********************************************/
#include "stdio.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Variable Declarations */
	float x = (float)1.1, y = (float)1.1;
	double x2 = 1.3, y2 = 1.4;
	
	/* Evaluae if statements */
	if( x == y ){
		fprintf(stdout, "The statement x==y is true\n");
	}
	
	if( x2 == y2 ){
		fprintf(stdout, "The statement x2==y2 is true\n");
	}
	

	return 0;
}

