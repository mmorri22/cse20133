/**********************************************
* File: color.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains an example of std::list, for 
* a Doubly Linked List
**********************************************/

#include<iostream> 
#include<string>
#include<list>

/* Foreground Colours */
const std::string SH_FG_BLACK 		   = "\033[0;30m";
const std::string SH_FG_BLUE           = "\033[0;34m";
const std::string SH_FG_GREEN          = "\033[0;32m";
const std::string SH_FG_CYAN           = "\033[0;36m";
const std::string SH_FG_RED            = "\033[0;31m";
const std::string SH_FG_PURPLE         = "\033[0;35m";
const std::string SH_FG_YELLOW         = "\033[0;33m";
const std::string SH_FG_LIGHT_GREY     = "\033[0;37m";
const std::string SH_FG_DARK_GREY      = "\033[1;30m";
const std::string SH_FG_LIGHT_BLUE     = "\033[1;34m";
const std::string SH_FG_LIGHT_GREEN    = "\033[1;32m";
const std::string SH_FG_LIGHT_CYAN     = "\033[1;36m";
const std::string SH_FG_LIGHT_RED      = "\033[1;31m";
const std::string SH_FG_LIGHT_PURPLE   = "\033[1;35m";
const std::string SH_FG_LIGHT_YELLOW   = "\033[1;33m";
const std::string SH_FG_WHITE          = "\033[1;37m";

/* Background Colours */
const std::string SH_BG_BLACK          = "\033[0;40m";
const std::string SH_BG_BLUE           = "\033[0;44m";
const std::string SH_BG_GREEN          = "\033[0;42m";
const std::string SH_BG_CYAN           = "\033[0;46m";
const std::string SH_BG_RED            = "\033[0;41m";
const std::string SH_BG_PURPLE         = "\033[0;45m";
const std::string SH_BG_YELLOW         = "\033[0;43m";
const std::string SH_BG_LIGHT_GREY     = "\033[0;47m";
const std::string SH_BG_DARK_GREY      = "\033[1;40m";
const std::string SH_BG_LIGHT_BLUE     = "\033[1;44m";
const std::string SH_BG_LIGHT_GREEN    = "\033[1;42m";
const std::string SH_BG_LIGHT_CYAN     = "\033[1;46m";
const std::string SH_BG_LIGHT_RED      = "\033[1;41m";
const std::string SH_BG_LIGHT_PURPLE   = "\033[1;45m";
const std::string SH_BG_LIGHT_YELLOW   = "\033[1;43m";
const std::string SH_BG_WHITE          = "\033[1;47m";

/* Others */
const std::string SH_DEFAULT           = "\033[0m";
const std::string SH_UNDERLINE         = "\033[4m";
const std::string SH_BLINK             = "\033[5m";
const std::string SH_INVERSE           = "\033[7m";
const std::string SH_CONCEALED         = "\033[8m";

const int numRows = 5;

void initializeBackground(std::list< std::list< std::string > >& background){
	
	std::list< std::string > color = {SH_BG_CYAN, SH_BG_CYAN, SH_BG_RED, SH_BG_RED, 
		SH_BG_PURPLE, SH_BG_PURPLE, SH_BG_GREEN, SH_BG_GREEN, SH_BG_CYAN, SH_BG_CYAN};

	
	for(int i = 0; i < numRows; i++){
		background.push_back(color);
		
		// Get the front element and save as temp
		std::string temp = color.front();
		// Delete the front element
		color.pop_front();
		// Put the temp element on the back 
		color.push_back(temp);
	}
}


void initializeRasterTransp(std::list< std::list< std::string > >& rasterTransp){
	
	std::list< std::string > row0 = {SH_BG_WHITE, SH_BG_WHITE, SH_BG_BLACK, SH_BG_WHITE, 
		SH_BG_WHITE, SH_BG_WHITE, SH_BG_BLACK, SH_BG_WHITE, SH_BG_WHITE, SH_BG_WHITE};
		
	std::list< std::string > row1 = {SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK, 
		SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK, SH_BG_WHITE, SH_BG_WHITE};
		
	std::list< std::string > row2 = {SH_BG_BLACK, SH_BG_WHITE, SH_BG_BLACK, SH_BG_WHITE, 
		SH_BG_BLACK, SH_BG_WHITE, SH_BG_BLACK, SH_BG_WHITE, SH_BG_BLACK, SH_BG_WHITE};
		
	std::list< std::string > row3 = {SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK, 
		SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK, SH_BG_WHITE, SH_BG_WHITE};
		
	std::list< std::string > row4 = {SH_BG_WHITE, SH_BG_WHITE, SH_BG_BLACK, SH_BG_WHITE, 
		SH_BG_WHITE, SH_BG_WHITE, SH_BG_BLACK, SH_BG_WHITE, SH_BG_WHITE, SH_BG_WHITE};
		
		
	rasterTransp.push_back(row0);
	rasterTransp.push_back(row1);
	rasterTransp.push_back(row2);
	rasterTransp.push_back(row3);
	rasterTransp.push_back(row4);
	
}


void initializeRasterOver(std::list< std::list< std::string > >& rasterOver){
	
	std::list< std::string > row0 = {SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLUE, SH_BG_BLACK, 
		SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLUE, SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK};
		
	std::list< std::string > row1 = {SH_BG_YELLOW, SH_BG_YELLOW, SH_BG_BLUE, SH_BG_BLUE, 
		SH_BG_YELLOW, SH_BG_YELLOW, SH_BG_BLUE, SH_BG_YELLOW, SH_BG_BLACK, SH_BG_BLACK};
		
	std::list< std::string > row2 = {SH_BG_YELLOW, SH_BG_BLACK, SH_BG_BLUE, SH_BG_BLACK, 
		SH_BG_BLUE, SH_BG_BLACK, SH_BG_BLUE, SH_BG_BLACK, SH_BG_YELLOW, SH_BG_BLACK};
		
	std::list< std::string > row3 = {SH_BG_YELLOW, SH_BG_YELLOW, SH_BG_BLUE, SH_BG_YELLOW, 
		SH_BG_YELLOW, SH_BG_BLUE, SH_BG_BLUE, SH_BG_YELLOW, SH_BG_BLACK, SH_BG_BLACK};
		
	std::list< std::string > row4 = {SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLUE, SH_BG_BLACK, 
		SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLUE, SH_BG_BLACK, SH_BG_BLACK, SH_BG_BLACK};
		
		
	rasterOver.push_back(row0);
	rasterOver.push_back(row1);
	rasterOver.push_back(row2);
	rasterOver.push_back(row3);
	rasterOver.push_back(row4);
	
}

void printRow(std::list< std::string >& theRow){
	
	for(std::string& colorVal : theRow){
		std::cout << colorVal << "  " << SH_DEFAULT;
	}
	std::cout << std::endl;
}


void printBlock(std::list< std::list< std::string > >& block){
	
	for( std::list< std::string >& theRow : block){
		printRow(theRow);
	}
	std::cout << std::endl;
}


void makeTransp(std::list< std::string >& color, std::list< std::string > rasterTransp){
	
	std::list< std::string >::iterator iter = rasterTransp.begin();
	
	for(std::string& colorVal : color){
		
		if(*iter == SH_BG_BLACK){
			colorVal = SH_BG_BLACK;
		}
		
		iter++;
	}
	
}


void makeBackgroundTransp(std::list< std::list< std::string > >& background, std::list< std::list< std::string > > rasterTransp){
	
	std::list< std::list< std::string > >::iterator iter = rasterTransp.begin();
	
	for(std::list< std::string >& row : background){
		makeTransp(row, *iter);
		iter++;
	}
	
}


void makeOver(std::list< std::string >& color, std::list< std::string > rasterOver){

	std::list< std::string >::iterator iter = rasterOver.begin();
	
	for(std::string& colorVal : color){
		
		if(*iter != SH_BG_BLACK){
			colorVal = *iter;
		}
		
		iter++;
	}

}


void makeBackgroundOver(std::list< std::list< std::string > >& background, std::list< std::list< std::string > > rasterOver){
	
	std::list< std::list< std::string > >::iterator iter = rasterOver.begin();
	
	for(std::list< std::string >& row : background){
		makeOver(row, *iter);
		iter++;
	}
	
}

int main(){
	
    std::cout << SH_BG_GREEN << "Showing an example of Gaming with Rasters" << SH_DEFAULT << std::endl;
    std::cout << SH_FG_YELLOW << "Here is the original designed background" << SH_DEFAULT << std::endl;	
	// Set Background List of Lists 
	std::list< std::list< std::string > > background;
	initializeBackground(background);
	printBlock(background);

    std::cout << SH_FG_LIGHT_CYAN << "Here is the transparency for the shape" << SH_DEFAULT << std::endl;		
	std::list< std::list< std::string > > rasterTransp;
	initializeRasterTransp(rasterTransp);
	printBlock(rasterTransp);
	
    std::cout << SH_FG_LIGHT_RED << "First, you use AND to make the transparency" << SH_DEFAULT << std::endl;
	makeBackgroundTransp(background, rasterTransp); 
	printBlock(background);

    std::cout << SH_FG_LIGHT_CYAN << "Here is the overlay for the shape" << SH_DEFAULT << std::endl;		
	std::list< std::list< std::string > > rasterOver;
	initializeRasterOver(rasterOver);
	printBlock(rasterOver);
	
    std::cout << SH_UNDERLINE << "Then, you use OR to do the overlay" << SH_DEFAULT << " Back To Normal Text" << std::endl;
	makeBackgroundOver(background, rasterOver); 
	printBlock(background);

	return 0;
}
