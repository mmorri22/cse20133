/**********************************************
* File: floatBad.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows a fundamental example of floats  
* This code fails because of implicit type conversion 
**********************************************/

#include "stdio.h"
#include "math.h"	/* Used for pow function */

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	/* Variable declarations */
	float smallFloat = -1.0125 * pow(2, -127);
	float slideFloat = 0.15625;
	float largeFloat = 1.8725 * pow(2, 127);

	return 0;
}

