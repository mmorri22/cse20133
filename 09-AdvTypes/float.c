/**********************************************
* File: float.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows a fundamental example of floats  
**********************************************/

#include "stdio.h"
#include "math.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){

	/* These type cast examples show the importance of understand the 
	* types of information. When the user types in -1.0125, the computing device 
	* automatically makes it a double, so the user has to cast it to a float 
	* in order to avoid implicit type conversion. */
	float smallFloat = (float)-1.0125 * (float)pow(2, -127);
	float slideFloat = (float)0.15625;
	float largeFloat = (float)1.8725 * (float)pow(2, 127);
	
	fprintf(stdout, "Decimal Representation of smallFloat is: %.45f\n", smallFloat);
	fprintf(stdout, "Scientific Notation of smallFloat is   : %e\n", smallFloat);
	fprintf(stdout, "Hex IEE 754 of smallFloat is           : %a\n\n", smallFloat);
	
	fprintf(stdout, "Decimal Representation of slideFloat is: %f\n", slideFloat);
	fprintf(stdout, "Scientific Notation of slideFloat is   : %e\n", slideFloat);
	fprintf(stdout, "Hex IEE 754 of slideFloat is           : %a\n\n", slideFloat);
	
	fprintf(stdout, "Decimal Representation of largeFloat is: %f\n", largeFloat);
	fprintf(stdout, "Scientific Notation of largeFloat is   : %e\n", largeFloat);
	fprintf(stdout, "Hex IEE 754 of largeFloat is           : %a\n\n", largeFloat);

	return 0;
}
