# Author: Matthew Morrison
# E-mail: matt.morrison@nd.edu
#
# This is the Makefile for the CSE 20133 course Lecture 10

# CC is for the GCC compiler for C 
CC := gcc

# CFLAGS are the compiler flages for when we compile C code in this course 
CFLAGS := -O0 -g -std=c11 -Wall -Wextra -Wshadow -Wconversion -pedantic -Werror 

# Variables for Folders
10 := ~/cse20133/10-Arrays


# Basic integer pointer
# Command: make charEx 
charEx: $(10)/charEx.o 
	$(CC) $(CFLAGS) -o $(10)/charEx $(10)/charEx.o

charEx.o: $(10)/charEx.c 
	$(CC) $(CFLAGS) -c $(10)/charEx.c
	


# Basic integer pointer
# Command: make intPtr 
intPtr: $(10)/intPtr.o 
	$(CC) $(CFLAGS) -o $(10)/intPtr $(10)/intPtr.o

intPtr.o: $(10)/intPtr.c 
	$(CC) $(CFLAGS) -c $(10)/intPtr.c



# Basic integer pointer with pedantic compiler errors
# Command: make intPtrBad 
intPtrBad: $(10)/intPtrBad.o 
	$(CC) $(CFLAGS) -o $(10)/intPtrBad $(10)/intPtrBad.o

intPtrBad.o: $(10)/intPtrBad.c 
	$(CC) $(CFLAGS) -c $(10)/intPtrBad.c



# Basic Memory Allocation
# Command: make malloc 
malloc: $(10)/malloc.o 
	$(CC) $(CFLAGS) -o $(10)/malloc $(10)/malloc.o

malloc.o: $(10)/malloc.c 
	$(CC) $(CFLAGS) -c $(10)/malloc.c
	
	
	
# Improper Memory Allocation example
# Command: make mallocBad
mallocBad: $(10)/mallocBad.o 
	$(CC) $(CFLAGS) -o $(10)/mallocBad $(10)/mallocBad.o

mallocBad.o: $(10)/mallocBad.c 
	$(CC) $(CFLAGS) -c $(10)/mallocBad.c



# Proper Memory Allocation example
# Fixes issue in mallocBad.c 
# Command: make mallocGood
mallocGood: $(10)/mallocGood.o 
	$(CC) $(CFLAGS) -o $(10)/mallocGood $(10)/mallocGood.o

mallocGood.o: $(10)/mallocGood.c 
	$(CC) $(CFLAGS) -c $(10)/mallocGood.c	
	
	
	
# Basic Memory Allocation with type abstraction
# Command: make mallAbst 
mallAbst: $(10)/mallAbst.o 
	$(CC) $(CFLAGS) -o $(10)/mallAbst $(10)/mallAbst.o

mallAbst.o: $(10)/mallAbst.c 
	$(CC) $(CFLAGS) -c $(10)/mallAbst.c



# Make all (intPtrBad not include because of deliberate compiler errors) 
all: charEx intPtr malloc mallocBad mallocGood mallAbst

# Make clean
clean :
	rm -rf *.o charEx intPtr intPtrBad malloc mallocBad mallocGood mallAbst



