/**********************************************
* File: spiral03.c
* Author: Matthew Morrison, Grace Hopper, Mark Dean
* Email: matt.morrison@nd.edu, ghopper@nd.edu, mdean@nd.edu
* 
* This contains the initial test for Coding Assignment 03 
* Test adding and subtracting values of money
**********************************************/

#include "bank.h"
#include "test.h"


/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(int argc, char** argv){
	
	/* Initialize Variables */
	char *firstName = "";
	char *lastName = "";
	long int custDollars;
	int custCents;
	
	// Test the inputs for correctness
	testInputs(argc, argv);
	
	// Initialize the Banker Info 
	initBankerInfo(firstName, lastName, &custDollars, &custCents, argv);
	
	// argv[5] is the dollars value for the deposit
	long int depositDollars = (long int)atoi(argv[5]);
	
	// argv[6] is the cents value for the deposit 
	int depositCents = atoi(argv[6]);
	
	// Make the deposit
	deposit(&custDollars, &custCents, depositDollars, depositCents);

	return 0;
}
