/**********************************************
* File: test.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the function declarations for
* the test functions for the bank functions 
**********************************************/

#ifndef TEST_H
#define TEST_H

#include "bank.h"

/********************************************
* Function Name  : test_checkCents
* Pre-conditions : none
* Post-conditions: none
* 
* This function tests the function checkCents 
********************************************/
void test_checkCents();


/********************************************
* Function Name  : test_printBankerInfo
* Pre-conditions : none
* Post-conditions: none
* 
* Tests the function printBankerInfo  
********************************************/
void test_printBankerInfo();


/********************************************
* Function Name  : testDeposit
* Pre-conditions : none
* Post-conditions: none
* 
* Tests if the inputs are correct for the program 
********************************************/
void testInputs(int argc, char**argv);


#endif
