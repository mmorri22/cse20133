/**********************************************
* File: bank.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the function declarations for
* the Coding Assignment 03 
**********************************************/

#ifndef BANK_H
#define BANK_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/********************************************
* Function Name  : checkCents
* Pre-conditions : int custCents
* Post-conditions: bool
* 
* This function checks if the cents is valid
********************************************/
bool checkCents(int custCents);


/********************************************
* Function Name  : printMoneyAmount
* Pre-conditions : long int custDollars, int custCents
* Post-conditions: none
* 
* Prints the values in terms of dollars and cents 
********************************************/
void printMoneyAmount(long int custDollars, int custCents);


/********************************************
* Function Name  : initBankerInfo
* Pre-conditions : char* FirstName, char* LastName, long int* custDollars, int* custCents, char** argv
* Post-conditions: none
* 
* This function initializes the baker information from a tested input command line 
********************************************/
void initBankerInfo(char* FirstName, char* LastName, long int* custDollars, int* custCents, char** argv);


/********************************************
* Function Name  : printBankerInfo
* Pre-conditions : char* FirstName, char* LastName, long int custDollars, int custCents
* Post-conditions: none
* 
* Prints the bankers' information to stdout  
********************************************/
void printBankerInfo(char* FirstName, char* LastName, long int custDollars, int custCents);


/********************************************
* Function Name  : deposit
* Pre-conditions : long int* custDollars, int* custCents, long int addDollars, int addCents
* Post-conditions: none
* 
* This function performs a transaction on money
* Accounts for addition - deposit
********************************************/
void deposit(long int* custDollars, int* custCents, long int addDollars, int addCents);


/********************************************
* Function Name  : withdrawl
* Pre-conditions : long int* custDollars, int* custCents, long int subDollars, int subCents
* Post-conditions: none
* 
* This function performs a transaction on money
* Accounts for subtraction - withdrawl
********************************************/
void withdrawl(long int* custDollars, int* custCents, long int subDollars, int subCents);

#endif
