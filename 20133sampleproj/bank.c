/**********************************************
* File: bank.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the function definitions for
* the bank functions for Coding Assignment 03
**********************************************/

#include "bank.h"

/********************************************
* Function Name  : checkCents
* Pre-conditions : int custCents
* Post-conditions: bool
* 
* This function checks if the cents is valid
********************************************/
bool checkCents(int custCents){
	
	if(custCents < 0 || custCents > 99){
		return false;
	}
	
	return true;
	
}


/********************************************
* Function Name  : printMoneyAmount
* Pre-conditions : long int custDollars, int custCents
* Post-conditions: none
* 
* Prints the values in terms of dollars and cents 
********************************************/
void printMoneyAmount(long int custDollars, int custCents){
	
		fprintf(stdout, "%ld.", custDollars);
		
		/* Account for if cents is less than */
		if(custCents < 10 && custCents > -10){
			fprintf(stdout, "0%d", custCents);
		}
		else{
			fprintf(stdout, "%d", custCents);
		}		
}


/********************************************
* Function Name  : printBankerInfo
* Pre-conditions : char* FirstName, char* LastName, long int custDollars, int custCents
* Post-conditions: none
* 
* Prints the bankers' information to stdout  
********************************************/
void printBankerInfo(char* FirstName, char* LastName, long int custDollars, int custCents){

	if(checkCents(custCents)){
		fprintf(stdout, "%s %s currently has ", FirstName, LastName);

		printMoneyAmount(custDollars, custCents);
		
		fprintf(stdout, " in their bank account\n");
		
	}
	else{
		fprintf(stderr, "The value of cents %d from %s %s is invalid\n",
				custCents, FirstName, LastName);
	}
}


/********************************************
* Function Name  : initBankerInfo
* Pre-conditions : char* FirstName, char* LastName, long int* custDollars, int* custCents, char** argv
* Post-conditions: none
* 
* This function initializes the baker information from a tested input command line 
********************************************/
void initBankerInfo(char* FirstName, char* LastName, long int* custDollars, int* custCents, char** argv){
	
	FirstName = argv[1];
	LastName = argv[2];
	*custDollars = (long int)atoi(argv[3]);
	*custCents = atoi(argv[4]);
	
	printBankerInfo(FirstName, LastName, *custDollars, *custCents);
	
}


/********************************************
* Function Name  : deposit
* Pre-conditions : long int* custDollars, int* custCents, long int addDollars, int addCents
* Post-conditions: none
* 
* This function performs a transaction on money
* Accounts for addition - deposit
********************************************/
void deposit(long int* custDollars, int* custCents, long int addDollars, int addCents){

	fprintf(stdout, "Proposed deposit to account of ");
	
	printMoneyAmount(addDollars, addCents);
	
	fprintf(stdout, "\n");

	if(checkCents(addCents) && addDollars > 0){
		
		*custCents += addCents;
		
		if(*custCents > 99){
			
			*custCents -= 100;
			*custDollars += 1;
		}
		
		*custDollars += addDollars;
		
		fprintf(stdout, "The values are now ");
		
		printMoneyAmount(*custDollars, *custCents);
	
		fprintf(stdout, "\n");
	
	}
	else{
		
		fprintf(stderr, "Deposit could not be made. Invalid inputs\n");
		
	}
	
}


/********************************************
* Function Name  : withdrawl
* Pre-conditions : long int* custDollars, int* custCents, long int subDollars, int subCents
* Post-conditions: none
* 
* This function performs a transaction on money
* Accounts for subtraction - withdrawl
********************************************/
void withdrawl(long int* custDollars, int* custCents, long int subDollars, int subCents){

	fprintf(stdout, "Proposed withdrawl to account of ");
	
	printMoneyAmount(subDollars, subCents);
	
	fprintf(stdout, "\n");

	if(checkCents(subCents) && subDollars > 0){
		
		*custCents -= subCents;
		
		if(*custCents < 99){
			
			*custCents *= -1;
			*custDollars -= 1;
		}
		
		*custDollars -= subDollars;
		
		fprintf(stdout, "The values are now ");
		
		printMoneyAmount(*custDollars, *custCents);
	
		fprintf(stdout, "\n");
	
	}
	else{
		
		fprintf(stderr, "Withdrawl could not be made. Invalid inputs\n");
		
	}
	
}