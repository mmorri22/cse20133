/**********************************************
* File: spiral02.c
* Author: Matthew Morrison, Grace Hopper, Mark Dean
* Email: matt.morrison@nd.edu, ghopper@nd.edu, mdean@nd.edu
* 
* This contains the initial test for Coding Assignment 03 
* Print the banker name and current money value to the 
* standard output
**********************************************/

#include "bank.h"
#include "test.h"

/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	test_printBankerInfo();
	
	char firstName[] = "Gary";
	char lastName[] = "Shadybanker";
	
	long int custDollars = 500;
	int custCents = 23;

	if(checkCents(custCents)){	
		printBankerInfo(firstName, lastName, custDollars, custCents);
	}
	else{
		fprintf(stderr, "Account invalid. %d is invalid cents\n", custCents);
	}

	return 0;
}

