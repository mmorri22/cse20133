/**********************************************
* File: test.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the function definitions for
* the test functions for Coding Assignment 03
**********************************************/

#include "test.h"

/********************************************
* Function Name  : test_checkCents
* Pre-conditions : none
* Post-conditions: none
* 
* This function tests the function checkCents 
********************************************/
void test_checkCents(){

	/* Allocate memory for 5 tests */
	long unsigned int numTests = 5;
	int* testVals = malloc(numTests * sizeof(int));

	testVals[0] = 0;
	*(testVals + 1) = 1;
	testVals[2] = 99;
	testVals[3] = 100;
	testVals[4] = -1;

	/* Go through all the tests to perform the tests */
	int iter = 0;
	for(iter = 0; iter < (int)numTests; ++iter){

		if(checkCents(testVals[iter])){
			fprintf(stdout, "%d is a valid cent value\n", testVals[iter]);
		}
		else{
			fprintf(stdout, "%d is not a valid cent value\n", testVals[iter]);
		}
	}
	
	// Free the allocated memory
	free(testVals);
}


/********************************************
* Function Name  : test_printBankerInfo
* Pre-conditions : none
* Post-conditions: none
* 
* Tests the function printBankerInfo  
********************************************/
void test_printBankerInfo(){
	
	printBankerInfo("Student1First", "Student1Last", 100, 8);
	printBankerInfo("Student2First", "Student2Last", 1842, 36);
	printBankerInfo("Student3First", "Student3Last", -18, 92);
	printBankerInfo("Student4First", "Student4Last", -18, 106);
}


/********************************************
* Function Name  : testDeposit
* Pre-conditions : none
* Post-conditions: none
* 
* Tests if the inputs are correct for the program 
********************************************/
void testInputs(int argc, char**argv){
	
	/* For this spiral, we are taking in a name, initial amount, deposit, and withdrawl */
	/* Also, the executable spiral03 */
	/* Therefore, we need 9 inputs */
	if(argc != 9){
		
		fprintf(stderr, "Incorrect inputs: ./spiral03 [First] [Last] [InitDollars] [InitCents] [DepositDollars] [DepositCents]\n");
		exit(-1);
		
	}
	
	/* Next, check inputs 3-6 for correct integers */
	/* If atoi returns 0, then argv[i][0] should be '0' argv[i][1] should be NULL */
	int iter;
	for(iter = 3; iter < argc; ++iter){
		
		/* Check for invalid input where there is an additional character */
		if(atoi(argv[iter]) == 0 && (argv[iter][0] != '0' || argv[iter][1] != 0) ){
			
			fprintf(stderr, "%s is not a valid integer\n", argv[iter]);
			exit(-1);
		}
		
		/* Check for accidental octal input */
		if(argv[iter][0] == '0' && argv[iter][1] != 0){
			
			fprintf(stderr, "%s is an accidental octal input\n", argv[iter]);
			exit(-1);
		}
	}
	
}
