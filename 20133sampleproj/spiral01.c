/**********************************************
* File: spiral01.c
* Author: Matthew Morrison, Grace Hopper, Mark Dean
* Email: matt.morrison@nd.edu, ghopper@nd.edu, mdean@nd.edu
* 
* This contains the initial test for Coding Assignment 03 
* First Push
**********************************************/

#include "bank.h"
#include "test.h"


/********************************************
* Function Name  : main
* Pre-conditions : void
* Post-conditions: int
* 
* This is the main driver function for the program 
********************************************/
int main(void){
	
	int custCents = 23;
	
	test_checkCents();
	
	checkCents(custCents);

	return 0;
}

