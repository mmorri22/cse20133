make clean
make all 
echo ""
echo "Spiral 01 test - Check Cents"
./spiral01
echo ""
echo "Spiral 02 test - Test Print Banker Information"
./spiral02
echo ""
echo "Spiral 03 test - No Inputs"
./spiral03
echo "Spiral 03 test - Too Many Inputs"
./spiral03 Gary Shadybanker 1 2 3 4 5 6 7 8 9
echo ""
echo "Spiral 03 test - Non Integer where ints should be"
./spiral03 Gary Shadybanker 100 23 Bubba 92 44 82
echo ""
echo "Spiral 03 test - Non Integer where ints should be"
./spiral03 Gary Shadybanker 100 23 0.ahd 92 44 82
echo ""
echo "Spiral 03 test - Catching accidental Octal input"
./spiral03 Gary Shadybanker 100 23 05 92 44 82
echo ""
echo "Spiral 03 test - Correct Inputs"
./spiral03 Gary Shadybanker 100 23 18 92 44 82
echo ""
echo "Spiral 03 test - Correct Inputs - 2"
./spiral03 Gary Shadybanker 100 23 101 24 44 82
echo ""
echo "Spiral 03 test - Correct Inputs - Add 77 to see if 23+77 gives 00 as the cents printout"
./spiral03 Gary Shadybanker 100 23 18 77 44 82
echo ""
echo "Spiral 03 test - Incorrect Inputs - Cents is a negative number"
./spiral03 Gary Shadybanker 100 23 18 -77 44 82



echo ""
echo "Final test - No Inputs"
./CA03
echo "Final test - Too Many Inputs"
./CA03 Gary Shadybanker 1 2 3 4 5 6 7 8 9
echo ""
echo "Final test - Non Integer where ints should be"
./CA03 Gary Shadybanker 100 23 Bubba 92 44 82
echo ""
echo "Final test - Non Integer where ints should be"
./CA03 Gary Shadybanker 100 23 0.ahd 92 44 82
echo ""
echo "Final test - Catching accidental Octal input"
./CA03 Gary Shadybanker 100 23 05 92 44 82
echo ""
echo "Final test - Correct Inputs"
./CA03 Gary Shadybanker 100 23 18 92 44 82
echo ""
echo "Final test - Correct Inputs - 2"
./CA03 Gary Shadybanker 100 23 101 24 44 82
echo ""
echo "Final test - Correct Inputs - Add 77 to see if 23+77 gives 00 as the cents printout"
./CA03 Gary Shadybanker 100 23 18 77 44 82
echo ""
echo "Final test - Incorrect Inputs - Cents is a negative number"
./CA03 Gary Shadybanker 100 23 18 77 44 -82

make clean