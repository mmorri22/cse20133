/* Circle.h */

#ifndef CIRCLE_H
#define CIRCLE_H

#include <iostream>

class Circle{


	private:
		double radius;

	public:
		Circle();

		Circle(double radIn);

		double getRadius() const;

		double getDiameter() const;

		double getArea() const;

		void setRadius(double inRad);

		bool operator==(const Circle &rhs);

		friend std::ostream& operator<<(std::ostream& output, const Circle& C);





};
#endif