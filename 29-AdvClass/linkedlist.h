/**********************************************
* File: linkedlist.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the function definitions and 
* structs for a Linked List Data structure 
**********************************************/

#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <cstdlib>
#include <iostream>


/* Node pointer typedef */
typedef struct node * NODEPTR;


/* NODE contains the data and a pointer to the next node */
typedef struct node
{
    int     data;
    NODEPTR next;    /* OR struct node *next; */
	
}NODE;


class LinkedList{
	
	private:
		NODEPTR head;
		
		/********************************************
		* Function Name  : CreateNode
		* Pre-conditions : void
		* Post-conditions: NODEPTR
		* 
		* CreateNode initializes the members
		* CreateNode exits if there is insufficient
		* memory
		********************************************/
		NODEPTR CreateNode (void) const;
		
		NODEPTR getHeadNode() const;
		
	public:
	
		LinkedList();

		/********************************************
		* Function Name  : Insert
		* Pre-conditions : int value
		* Post-conditions: void   
		* 
		* the node is inserted at the end of the linked list
		********************************************/
		void    Insert     (int value);


		/********************************************
		* Function Name  : Delete
		* Pre-conditions : int data
		* Post-conditions: int    
		*  
		* Delets the first instance of data. Returns -1 
		* if not found
		********************************************/
		int     Delete     (int data);


		/********************************************
		* Function Name  : IsEmpty
		* Pre-conditions : NODEPTR head
		* Post-conditions: int    
		* 
		* returns 1 (true) if the list is empty
		* returns 0 (false) if the list is not empty
		********************************************/
		bool     IsEmpty    () const;


		/********************************************
		* Function Name  : PrintList
		* Pre-conditions : NODEPTR head
		* Post-conditions: void    
		* 
		* each node in the list is printed according to the format specified in this code
		********************************************/
		friend std::ostream& operator<<( std::ostream& output, const LinkedList& theList );

};

#endif
