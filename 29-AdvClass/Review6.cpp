#include <iostream>

void PrintInt (int i) {
   i = i + 2;
   std::cout << i << std::endl;
}

void PrintInt (int& i) {
   i = i + 2;
   std::cout << i << std::endl;
}

void PrintInt (const int i) {
   i = i + 2;
   std::cout << i << std::endl;
}

int main(){
 int tempX = 25;
 PrintInt(tempX);
 std::cout << tempX << std::endl;
 return 0;
}
