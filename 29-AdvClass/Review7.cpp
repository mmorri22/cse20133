#include <iostream>

double Func(double d, int n){

	if(n == 0){
		return 1;
	}

	else if(n < 0){
		return d * Func(d, n+1);
	}
	
	else{  // n > 0
			return d * Func(d, n-1);
	}
}

int main(){

	std::cout << Func(3, 4) << std::endl;

}