#include <iostream>

void func1(float* tempX, float* tempY){
 
 std::cout << &tempX << " " << &tempY << std::endl;
 std::cout << tempX << " " << tempY << std::endl;
 std::cout << *tempX << " " << *tempY << std::endl;
 std::cout << std::endl;
}


void func2(float& tempX, float& tempY){

 std::cout << &tempX << " " << &tempY << std::endl;
 std::cout << tempX << " " << tempY << std::endl;
 std::cout << std::endl;

}

int main(){

 float tempX = (float)1.3;
 float tempY = (float)2.6;
 float *ptrX, *ptrY;
 
 ptrX = &tempX;
 ptrY = &tempY;
 
 func1(ptrX, ptrY);
 
 func2(tempX, tempY);
 
 return 0;
}