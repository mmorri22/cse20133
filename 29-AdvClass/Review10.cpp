#include <iostream>
#include <string>
#include <vector>

template<class T>
void printVecElems( std::vector< T > theVec ){
	
	for(unsigned int i = 0; i < theVec.size(); ++i){
		
		std::cout << theVec.at(i) << " ";
		
	}
	
	std::cout << std::endl;
}

int main(){

	std::vector< std::string > name;
	
	name.push_back("Here"); name.push_back("We"); name.push_back("Go");
	name.push_back("Irish"); name.push_back("Moth"); name.push_back("Here We Go");
	
	printVecElems(name);
	name.erase(name.begin() + 4);
	printVecElems(name);
	
	std::vector< int > integers(5, -5);
	
	for(int i = 0; i < 4; ++i){
		integers.at(i) = 2*i;
	}
	
	printVecElems(integers);
	
	integers.push_back(1842);
	
	printVecElems(integers);
		
	return 0;
}