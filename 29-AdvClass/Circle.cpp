/* Circle.cpp */

#include "Circle.h"

Circle::Circle() : radius(1) {}

Circle::Circle(double radIn) : radius(radIn) {}

double Circle::getRadius() const{
	return radius;
}

double Circle::getDiameter() const{
	return 2*radius;
}

double Circle::getArea() const{
	return 3.14159*radius;
}

void Circle::setRadius(double inRad){
	radius = inRad;
}

bool Circle::operator==(const Circle& rhs) const{
	
	if(radius == rhs.radius){
		return true;
	}
	return false;
}

std::ostream& operator<<(std::ostream& output, const Circle& C){
	output << radius;
	return output;
}