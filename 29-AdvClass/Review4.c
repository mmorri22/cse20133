#include <stdio.h>
#include <stdlib.h> /* Included for the exit function */

int main(int argc, char** argv){
	
	if(argc != 2){
		fprintf(stderr, "Input Format: ./filePtrPrint [File Name]\n");
		exit(-1);
	}

	FILE *fp1 = fopen(argv[1], "r");

	if(fp1 == NULL){
		fprintf(stderr, "The file %s does not exist\n", argv[1]);
		exit(-1);
	}
	else{
		
		int tempChar;
		
		while( (tempChar = fgetc(fp1)) != EOF ){
			
			if(tempChar != '\n'){
				fprintf(stdout, "%c\n", tempChar);
			}
		}
		
		fclose(fp1);
	}

	return 0;
}
