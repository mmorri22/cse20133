/**********************************************
* File: linkedlist.c
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This file contains the member definitions
* for a Linked List Class 
**********************************************/

#include "linkedlist.h"


LinkedList::LinkedList() : head(NULL) {}


/********************************************
* Function Name  : CreateNode
* Pre-conditions : void
* Post-conditions: NODEPTR
* 
* CreateNode initializes the members
* CreateNode exits if there is insufficient
* memory
********************************************/
NODEPTR LinkedList::CreateNode (void) const
{
   NODEPTR newNode;

   /* Get the space needed for the node */
   newNode = (NODEPTR) malloc (sizeof(NODE));
   if (newNode == NULL)
   {
      std::cerr <<  "Out of Memory - CreateNode" << std::endl;
      exit (-1);
   }

   /* Initialize the members */
   newNode -> data = 0;
   newNode -> next = NULL;

   return newNode;
}


NODEPTR LinkedList::getHeadNode() const{
	return head;
}

/********************************************
* Function Name  : Insert
* Pre-conditions : int value
* Post-conditions: void   
* 
* the node is inserted at the end of the linked list
********************************************/
void LinkedList::Insert (int value)
{
	
   NODEPTR temp = CreateNode();
   temp -> data = value;
   
   NODEPTR prev, curr;

   if ( IsEmpty ())
   {
       head = temp;
   }
   else
   {
      prev = NULL;
      curr = head;

      /* traverse the list until the end */
      while (curr != NULL)
      {
		 prev = curr;
		 curr = curr -> next;
      }
  
      /* insert the node, temp, at the end */
      prev -> next = temp;
   }
}


/********************************************
* Function Name  : Delete
* Pre-conditions : int data
* Post-conditions: int    
*  
* Delets the first instance of data. Returns -1 
* if not found
********************************************/
int LinkedList::Delete (int target)
{
   int value;
   NODEPTR temp, prev, curr;

   if (IsEmpty ())
   {
      std::cout << "Can't delete from an empty list" << std::endl;
      return (-1);
   }

   /* if the target value is the first
   in the list, move head */
   else if (target == head -> data)
   {
      temp = head;
      value = head -> data;
      head = head -> next;
      free (temp);
      return (value);
   }
   
   /* traverse the list until the 
   target value is found */
   else
   {
      prev = head;
      curr = head -> next;

      while (curr != NULL &&
	     curr -> data != target)
      {
		 prev = curr;
		 curr = curr -> next;
      }
      
      if(curr != NULL)
      {
	
		/* delete the node the contains
		the target value */
		temp = curr;
		prev -> next = curr -> next;
		value = curr -> data;
		free(temp);
		return (value);
      }
      else
      {
		std::cout << target << " was not in the list" << std::endl;
		return (-1);
      }
   }      
}


/********************************************
* Function Name  : IsEmpty
* Pre-conditions : void
* Post-conditions: int    
* 
* returns 1 (true) if the list is empty
* returns 0 (false) if the list is not empty
********************************************/
bool LinkedList::IsEmpty () const
{
   /* If the pointer to the list is
   NULL then there is no list.  The
   list is empty, so we return true.
   */
   return (head == NULL);
}


std::ostream& operator<<( std::ostream& output, const LinkedList& theList )
{
   NODEPTR curr;

   if (theList.IsEmpty())
   {
	  output << "The list is empty" << std::endl;;
   }
   else
   {
	  /* set the current pointer to the first
	  ** node of the list */
	  curr = theList.getHeadNode();

	  /* Until the end of the list */
	  while (curr != NULL)
	  {
		  /* print the current data item */
		  output << curr->data << " ";

		  /* move to the next node */
		  curr = curr -> next;
	  }
   } 

	return output;
}