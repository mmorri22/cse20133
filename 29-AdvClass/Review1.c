#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv){
	
	if(argc < 2){
		fprintf(stderr, "Not enough inputs on command line\n");
		exit(-1);
	}
	
	int i = 1;
	while(i < argc){
		fprintf(stdout, "argv[%d] = %s\n", i, *(argv + i));
		i++;
	}
	
	return 0;
}